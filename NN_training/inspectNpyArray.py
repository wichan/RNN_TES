#!/usr/bin/env python

import sys
from numpy import count_nonzero
from modules.numpy_utils import np_load

print "Loading numpy array '{}'...".format(sys.argv[1])
array = np_load(sys.argv[1])
print "The loaded array has\n  {} entries".format(len(array))
if "n_tracks" in array.dtype.names:
    print "  (one-prong: {}, multi-prong: {})".format(
        count_nonzero(array["n_tracks"]==1),
        count_nonzero(array["n_tracks"]>1))
if "reco_decay_mode" in array.dtype.names:
    print "  (1p0n: {}, 1p1n: {}, 1pXn: {}, 3p0n: {}, 3pXn: {})".format(
        count_nonzero(array["reco_decay_mode"]==0),
        count_nonzero(array["reco_decay_mode"]==1),
        count_nonzero(array["reco_decay_mode"]==2),
        count_nonzero(array["reco_decay_mode"]==3),
        count_nonzero(array["reco_decay_mode"]==4))
print "and the following fields:"
for f in array.dtype.names:
    print "  {}".format(f)
