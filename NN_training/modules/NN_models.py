#!/usr/bin/env python

import os
import numpy as np

from tensorflow.python.framework.ops import Tensor

#from keras.models import Sequential
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Masking, TimeDistributed, LSTM, concatenate
from keras.optimizers import Adam, RMSprop
from keras.utils import plot_model
from keras.callbacks import EarlyStopping

from custom_objects import get_activations, get_objectives, load_model
from general_utils import makeDirs


#-------------------------------------------------------------------------------
# Abstract class for all NN models
class NNModel(object):
    def __init__(self, loss='mean_squared_error', epochs=100, batch_size=32, optimizer='adam', learning_rate=1e-4, lr_decay=0):
        self.epochs = epochs
        self.batch_size = batch_size
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.lr_decay = lr_decay

        self.model = None
        self.compiled = False
        self.history = None

        try:
            self.loss = get_objectives(loss)
        except:
            raise Exception("{}.__init__(): loss function not defined in keras.objectives or modules.custom_objects.".format(self.__class__.__name__))

    def cloneFrom(self, other):
        self.epochs = other.epochs
        self.batch_size = other.batch_size
        self.optimizer = other.optimizer
        self.learning_rate = other.learning_rate
        self.lr_decay = other.lr_decay

        self.model = None
        self.compiled = False
        self.history = None

        try:
            self.loss = get_objectives(other.loss)
        except:
            raise Exception("{}.__init__(): loss function not defined in keras.objectives or modules.custom_objects.".format(self.__class__.__name__))

    def clone(self):
        new = self.__class__()
        new.cloneFrom(self)
        return new

    def build(self):
        raise NotImplementedError("{}.build(): NN architecture not defined/implemented.".format(self.__class__.__name__))

    def compile(self, print_summary=True):
        if self.model is None:
            self.build()

        if self.optimizer.lower() == 'adam':
            adam = Adam(lr=self.learning_rate, decay=self.lr_decay)
            self.model.compile(loss=self.loss, optimizer=adam)
        elif self.optimizer.lower() == 'rmsprop':
            rmsprop = RMSprop(lr=self.learning_rate, decay=self.lr_decay)
            self.model.compile(loss=self.loss, optimizer=rmsprop)
        else:
            self.model.compile(loss=self.loss, optimizer=self.optimizer)

        if print_summary:
            print self.model.summary()

        self.compiled = True

    def fit(self, input, target, sample_weight=None, validation_data=None, **kwargs):
        if not self.compiled:
            self.compile()

        self.history = self.model.fit(input, target, sample_weight=sample_weight, validation_data=validation_data, epochs=self.epochs, batch_size=self.batch_size, verbose=2, **kwargs)

        return self.history

    def predict(self, input):
        return self.model.predict(input)

    def save(self, name, save_visualised_figure=True):
        if os.path.exists(name):
            print "WARNING: Replacing existing file '{}'".format(name)
            os.remove(name)
        makeDirs(os.path.split(name)[0])
        self.model.save(name)

        if save_visualised_figure:
            visualised_eps = os.path.splitext(name)[0]+"_visualised.eps"
            if os.path.exists(visualised_eps):
                print "WARNING: Replacing existing file '{}'".format(visualised_eps)
                os.remove(visualised_eps)
            plot_model(self.model, to_file=visualised_eps, show_shapes=True)

    def load(self, name):
        self.model = load_model(name)
        self.compiled = False

#-------------------------------------------------------------------------------
# Simple dense model (for emulating BDT)
class NNDense(NNModel):
    def __init__(self, input=None, nodes=[128,128], actv='relu', output_actv='linear', dropout=0, **kwargs):
        super(NNDense, self).__init__(**kwargs)

        self.input = input
        self.nodes = nodes
        self.n_layers = len(nodes)
        self.dropout = dropout

        self.layers = []

        try:
            self.actv = get_activations(actv)
            self.output_actv = get_activations(output_actv)
        except:
            raise Exception("{}.__init__(): activation function not defined in keras.activations or modules.custom_objects.".format(self.__class__.__name__))

    def cloneFrom(self, other):
        super(NNDense, self).cloneFrom(other)

        self.input = other.input
        self.nodes = other.nodes
        self.n_layers = len(other.nodes)
        self.dropout = other.dropout

        self.layers = []

        try:
            self.actv = get_activations(other.actv)
            self.output_actv = get_activations(other.output_actv)
        except:
            raise Exception("{}.__init__(): activation function not defined in keras.activations or modules.custom_objects.".format(self.__class__.__name__))

    def build(self):
        # Input layer
        if isinstance(self.input, int):
            input_layer = Input(shape = (self.input,))
        elif isinstance(self.input, Tensor):
            input_layer = self.input
        else:
            raise ValueError("{}.build(): Unknown input layer format.".format(self.__class__.__name__))
        self.layers.append(input_layer)

        # Hidden layers (and dropout)
        for n in self.nodes:
            hidden_layer = Dense(n, activation=self.actv)(self.layers[-1])
            self.layers.append(hidden_layer)
            if self.dropout:
                dropout = Dropout(self.dropout)(self.layers[-1])
                self.layers.append(dropout)

        # Output layer
        output_layer = Dense(1, activation=self.output_actv)(self.layers[-1])
        self.layers.append(output_layer)

        model = Model(input=self.layers[0], output=self.layers[-1])
        self.model = model

#-------------------------------------------------------------------------------
# NN with branches of dense and LSTM layers concatenated
# Assumed structure:
# Branch 1: Input (scalars) -> Dense layers -> Output 1
# Branch 2: Input (sequences) -> Time-distributed dense layers -> LSTM layers -> Output 2
# Branch 3: Input (sequences) -> Time-distributed dense layers -> LSTM layers -> Output 3
# Merged: Output 1 + Output 2 + Output 3 -> Dense layers -> Final output
# Branch 3 is optional (dropped if branch3_input=None)
class NNDensePlusLSTM(NNModel):
    def __init__(self,
                 branch1_input=None,
                 branch1_dense_nodes=None, branch1_dense_actv='relu',
                 branch2_input=None, branch2_mask=-999.,
                 branch2_dense_nodes=None, branch2_dense_actv='relu',
                 branch2_LSTM_nodes=None, branch2_LSTM_actv='relu',
                 branch3_input=None, branch3_mask=-999.,
                 branch3_dense_nodes=None, branch3_dense_actv='relu',
                 branch3_LSTM_nodes=None, branch3_LSTM_actv='relu',
                 merged_dense_nodes=None, merged_dense_actv='relu',
                 output_actv='linear',
                 **kwargs):
        super(NNDensePlusLSTM, self).__init__(**kwargs)

        self.branch1_input       = branch1_input
        self.branch1_dense_nodes = branch1_dense_nodes
        self.branch2_input       = branch2_input
        self.branch2_mask        = branch2_mask
        self.branch2_dense_nodes = branch2_dense_nodes
        self.branch2_LSTM_nodes  = branch2_LSTM_nodes
        self.branch3_input       = branch3_input
        self.branch3_mask        = branch3_mask
        self.branch3_dense_nodes = branch3_dense_nodes
        self.branch3_LSTM_nodes  = branch3_LSTM_nodes
        self.merged_dense_nodes  = merged_dense_nodes

        self.branch1_layers = []
        self.branch2_layers = []
        self.branch3_layers = []
        self.merged_layers = []

        try:
            self.branch1_dense_actv = get_activations(branch1_dense_actv)
            self.branch2_dense_actv = get_activations(branch2_dense_actv)
            self.branch2_LSTM_actv = get_activations(branch2_LSTM_actv)
            self.branch3_dense_actv = get_activations(branch3_dense_actv)
            self.branch3_LSTM_actv = get_activations(branch3_LSTM_actv)
            self.merged_dense_actv = get_activations(merged_dense_actv)
            self.output_actv = get_activations(output_actv)
        except:
            raise Exception("{}.__init__(): activation function not defined in keras.activations or modules.custom_objects.".format(self.__class__.__name__))

    def cloneFrom(self, other):
        super(NNDensePlusLSTM, self).cloneFrom(other)

        self.branch1_input       = other.branch1_input
        self.branch1_dense_nodes = other.branch1_dense_nodes
        self.branch2_input       = other.branch2_input
        self.branch2_mask        = other.branch2_mask
        self.branch2_dense_nodes = other.branch2_dense_nodes
        self.branch2_LSTM_nodes  = other.branch2_LSTM_nodes
        self.branch3_input       = other.branch3_input
        self.branch3_mask        = other.branch3_mask
        self.branch3_dense_nodes = other.branch3_dense_nodes
        self.branch3_LSTM_nodes  = other.branch3_LSTM_nodes
        self.merged_dense_nodes  = other.merged_dense_nodes

        self.branch1_layers = []
        self.branch2_layers = []
        self.branch3_layers = []
        self.merged_layers = []

        try:
            self.branch1_dense_actv = get_activations(other.branch1_dense_actv)
            self.branch2_dense_actv = get_activations(other.branch2_dense_actv)
            self.branch2_LSTM_actv = get_activations(other.branch2_LSTM_actv)
            self.branch3_dense_actv = get_activations(other.branch3_dense_actv)
            self.branch3_LSTM_actv = get_activations(other.branch3_LSTM_actv)
            self.merged_dense_actv = get_activations(other.merged_dense_actv)
            self.output_actv = get_activations(other.output_actv)
        except:
            raise Exception("{}.__init__(): activation function not defined in keras.activations or modules.custom_objects.".format(self.__class__.__name__))

    def build(self):
        # Branch 1
        input_layer = None
        if isinstance(self.branch1_input, int):
            input_layer = Input(shape = (self.branch1_input,))
        elif isinstance(self.branch1_input, tuple):
            if len(self.branch1_input) == 1:
                input_layer = Input(shape = self.branch1_input)
            elif len(self.branch1_input) == 2:
                input_layer = Input(shape = self.branch1_input[1:])
        elif isinstance(self.branch1_input, Tensor):
            input_layer = self.branch1_input

        if input_layer is None:
            raise ValueError("{}.build(): Unknown input layer format.".format(self.__class__.__name__))
        else:
            self.branch1_layers.append(input_layer)

        for n in self.branch1_dense_nodes:
            dense = Dense(n, activation=self.branch1_dense_actv)(self.branch1_layers[-1])
            self.branch1_layers.append(dense)

        # Branch 2
        input_layer = None
        if isinstance(self.branch2_input, tuple):
            if len(self.branch2_input) == 2:
                input_layer = Input(shape = self.branch2_input)
            elif len(self.branch2_input) == 3:
                input_layer = Input(shape = self.branch2_input[1:])
        elif isinstance(self.branch2_input, Tensor):
            input_layer = self.branch2_input

        if input_layer is None:
            raise ValueError("{}.build(): Unknown input layer format.".format(self.__class__.__name__))
        else:
            self.branch2_layers.append(input_layer)

        if self.branch2_mask is not None:
            masking = Masking(mask_value=self.branch2_mask)(self.branch2_layers[-1])
            self.branch2_layers.append(masking)

        for n in self.branch2_dense_nodes:
            dense = Dense(n, activation=self.branch2_dense_actv)
            time_distributed_dense = TimeDistributed(dense)(self.branch2_layers[-1])
            self.branch2_layers.append(time_distributed_dense)

        lstm = LSTM(n, activation=self.branch2_LSTM_actv,
                    unroll=True, go_backwards=False, return_sequences=True)(self.branch2_layers[-1])
        self.branch2_layers.append(lstm)
        for n in self.branch2_LSTM_nodes[1:]:
            lstm = LSTM(n, activation=self.branch2_LSTM_actv,
                        unroll=True, go_backwards=False)(self.branch2_layers[-1])
            self.branch2_layers.append(lstm)

        # Branch 3
        if self.branch3_input is not None:
            input_layer = None
            if isinstance(self.branch3_input, tuple):
                if len(self.branch3_input) == 2:
                    input_layer = Input(shape = self.branch3_input)
                elif len(self.branch3_input) == 3:
                    input_layer = Input(shape = self.branch3_input[1:])
            elif isinstance(self.branch3_input, Tensor):
                input_layer = self.branch3_input

            if input_layer is None:
                raise ValueError("{}.build(): Unknown input layer format.".format(self.__class__.__name__))
            else:
                self.branch3_layers.append(input_layer)

            if self.branch3_mask is not None:
                masking = Masking(mask_value=self.branch3_mask)(self.branch3_layers[-1])
                self.branch3_layers.append(masking)

            for n in self.branch3_dense_nodes:
                dense = Dense(n, activation=self.branch3_dense_actv)
                time_distributed_dense = TimeDistributed(dense)(self.branch3_layers[-1])
                self.branch3_layers.append(time_distributed_dense)

            lstm = LSTM(n, activation=self.branch3_LSTM_actv,
                        unroll=True, go_backwards=False, return_sequences=True)(self.branch3_layers[-1])
            self.branch3_layers.append(lstm)
            for n in self.branch3_LSTM_nodes[1:]:
                lstm = LSTM(n, activation=self.branch3_LSTM_actv,
                            unroll=True, go_backwards=False)(self.branch3_layers[-1])
                self.branch3_layers.append(lstm)

        # Merged
        if len(self.branch3_layers) > 0:
            merged = concatenate([self.branch1_layers[-1], self.branch2_layers[-1], self.branch3_layers[-1]])
        else:
            merged = concatenate([self.branch1_layers[-1], self.branch2_layers[-1]])
        self.merged_layers.append(merged)

        for n in self.merged_dense_nodes:
            dense = Dense(n, activation=self.merged_dense_actv)(self.merged_layers[-1])
            self.merged_layers.append(dense)

        # Output layer
        output_layer = Dense(1, activation=self.output_actv)(self.merged_layers[-1])
        self.merged_layers.append(output_layer)

        # Create Keras Model
        inputs = [self.branch1_layers[0], self.branch2_layers[0]]
        if len(self.branch3_layers) > 0:
            inputs.append(self.branch3_layers[0])
        model = Model(input=inputs,
                      output=self.merged_layers[-1])
        self.model = model
