import numpy as np
from numpy.lib.recfunctions import drop_fields
from numpy_utils import toNonStructured, toNonStructuredSequences

#-------------------------------------------------------------------------------
def setTensorFlowThreads(n_threads):
    from keras import backend as K
    import tensorflow.compat.v1 as tf

    config = tf.ConfigProto(intra_op_parallelism_threads=n_threads,
                            inter_op_parallelism_threads=n_threads,
                            allow_soft_placement=True,
                            device_count = {'CPU': n_threads})
    session = tf.Session(config=config)
    K.set_session(session)

#-------------------------------------------------------------------------------
def findScaling(arr, vars=None):
    if vars is None:
        vars = arr.dtype.names
    scaling = {}
    for var in vars:
        if arr[var].dtype == 'O':
            a = np.hstack(arr[var])
        else:
            a = arr[var]
        scaling[var] = {'mean': np.mean(a), 'std': np.std(a)}
        if scaling[var]['std'] == 0:
            print "WARNING: variable '{}' has zero deviation (i.e. a constant), you should drop this variable if it's an input.".format(var)
    return scaling

#-------------------------------------------------------------------------------
def applyScaling(arr, scaling=None):
    if scaling is None:
        scaling = findScaling(arr)
    for var in arr.dtype.names:
        if var not in scaling:
            continue
        arr[var] = (arr[var] - scaling[var]['mean']) / scaling[var]['std']
    return arr

#-------------------------------------------------------------------------------
def revertScaling(arr, scaling, var=None):
    if var is None:
        # `arr` is a structured array and `scaling` is a dict of different variables' scalings
        for var in arr.dtype.names:
            if var not in scaling:
                continue
            arr[var] = (arr[var] * scaling[var]['std']) + scaling[var]['mean']
        return arr
    else:
        # `arr` is a non-structured array for one variable and `scaling` is that variable's scaling
        return (arr * scaling[var]['std']) + scaling[var]['mean']

#-------------------------------------------------------------------------------
def ptToPtRatio(arr, base="combined_pt", keep_raw_truth=False):
    for field in arr.dtype.names:
        if not field.endswith("_pt"):
            continue
        if field == base:
            continue
        if field.startswith("cls_") or field.startswith("trk_"):
            continue
        if keep_raw_truth and "truth" in field:
            continue
        arr[field] = arr[field] / arr[base]

#-------------------------------------------------------------------------------
def ptRatioToPt(arr, base="combined_pt", kept_raw_truth=False):
    for field in arr.dtype.names:
        if not field.endswith("_pt"):
            continue
        if field == base:
            continue
        if field.startswith("cls_") or field.startswith("trk_"):
            continue
        if kept_raw_truth and "truth" in field:
            continue
        arr[field] = arr[field] * arr[base]

#-------------------------------------------------------------------------------
def selectTauDecayMode(arr, decay_mode):
    if decay_mode == "1p":
        arr = arr[arr["n_tracks"]==1]
    elif decay_mode == "3p":
        arr = arr[arr["n_tracks"]==3]
    elif decay_mode == "mp":
        arr = arr[arr["n_tracks"]>1]
    elif decay_mode == "1p0n":
        arr = arr[arr["reco_decay_mode"]==0]
    elif decay_mode == "1p1n":
        arr = arr[arr["reco_decay_mode"]==1]
    elif decay_mode == "1pXn":
        arr = arr[arr["reco_decay_mode"]==2]
    elif decay_mode == "3p0n":
        arr = arr[arr["reco_decay_mode"]==3]
    elif decay_mode == "3pXn":
        arr = arr[arr["reco_decay_mode"]==4]
    return arr

#-------------------------------------------------------------------------------
### HARD-CODING!!
# decay-mode-dependent variable drop
def dropVariables(input_vars, decay_mode):
    if "1p" in decay_mode:
        input_vars.remove("pantauBDT_3p0n_vs_3pXn")
        if decay_mode == "1p0n":
            input_vars.remove("pantauBDT_1p1n_vs_1pXn")
        elif decay_mode == "1pXn":
            input_vars.remove("pantauBDT_1p0n_vs_1p1n")

    if "3p" in decay_mode or decay_mode == "mp":
        input_vars.remove("pantauBDT_1p0n_vs_1p1n")
        input_vars.remove("pantauBDT_1p1n_vs_1pXn")

    if "0n" in decay_mode:
        input_vars.remove("Upsilon")  # This becomes constant one without neutral PFOs

    return input_vars  # Not necessary but doesn't hurt (lists are mutable)

#-------------------------------------------------------------------------------
def splitTargetInput(arr, target, inputs, sequence_inputs_1=None, sequence_inputs_2=None):
    arr_target = arr[target].astype(float)

    vars_to_drop = [v for v in arr.dtype.names if v not in inputs]
    arr_inputs = drop_fields(arr, vars_to_drop, usemask=False)
    arr_inputs = toNonStructured(arr_inputs)

    if not sequence_inputs_1:
        return arr_target, arr_inputs

    vars_to_drop = [v for v in arr.dtype.names if v not in sequence_inputs_1]
    arr_seq_inputs_1 = drop_fields(arr, vars_to_drop, usemask=False)
    arr_seq_inputs_1 = toNonStructuredSequences(arr_seq_inputs_1)

    if not sequence_inputs_2:
        return arr_target, arr_inputs, arr_seq_inputs_1

    vars_to_drop = [v for v in arr.dtype.names if v not in sequence_inputs_2]
    arr_seq_inputs_2 = drop_fields(arr, vars_to_drop, usemask=False)
    arr_seq_inputs_2 = toNonStructuredSequences(arr_seq_inputs_2)

    return arr_target, arr_inputs, arr_seq_inputs_1, arr_seq_inputs_2
