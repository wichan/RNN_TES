import os
import yaml
from general_utils import find

#-------------------------------------------------------------------------------
# Class that loads `selections.yaml` into a dict of `Selection`s
class Selections(dict):
    def __init__(self, file_name):
        if file_name is None:
            file_name = find(os.environ, "TES_SELECTIONS", "configs/selections.yaml")

        with open(file_name, "r") as f:
            selections = yaml.load(f, Loader=yaml.FullLoader)
        
        for name,selection in selections.iteritems():
            self[name] = Selection(selection)
    
    def __repr__(self):
        return "<{}.{} object at {}>: {:d} selections ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self),
        ", ".join(self.keys()) )

#-------------------------------------------------------------------------------
# Class that holds all the names of the variables that should be kept in thinning
class Selection(object):
    def __init__(self, selection):
        self.variables = selection["variables"]
        self.expression = selection["expression"]
    
    def __call__(self, arr):
        fields = ["arr['%s']" % var for var in self.variables]
        #print self.expression.format(*fields)
        exec "mask = %s" % self.expression.format(*fields)
        return arr[mask]
    
    def __str__(self):
        return self.expression.format(self.variables)
    
    def __repr__(self):
        return "<{}.{} object at {}>: {}".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        str(self) )
