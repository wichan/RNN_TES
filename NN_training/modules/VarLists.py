import os
import yaml
from general_utils import find

#-------------------------------------------------------------------------------
# Class that loads `varlists.yaml` into a dict of `VarList`s
class VarLists(dict):
    def __init__(self, file_name=None):
        if file_name is None:
            file_name = find(os.environ, "TES_THINNINGS", "configs/varlists.yaml")

        with open(file_name, "r") as f:
            lists = yaml.load(f, Loader=yaml.FullLoader)

        for name,l in lists.iteritems():
            self[name] = VarList(l)

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} variable lists ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self),
        ", ".join(self.keys()) )

#-------------------------------------------------------------------------------
# Class that holds lists of name of variables, grouped in categories (keys)
class VarList(dict):
    def __init__(self, l):
        super(VarList, self).__init__(l)

    def listAll(self):
        all_vars = []
        for vars in self.values():
            all_vars += vars
        return list(set(all_vars))

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} variables ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self.listAll()),
        ", ".join(self.listAll()) )
