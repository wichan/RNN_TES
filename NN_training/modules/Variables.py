import os
import re
import yaml
import numpy as np
from general_utils import find

#-------------------------------------------------------------------------------
# Class that loads `variables.yaml` into a set of `Variable`s
class Variables(dict):

    def __init__(self, file_name=None):
        if file_name is None:
            file_name = find(os.environ, "TES_VARIABLES", "configs/variables.yaml")

        with open(file_name, "r") as f:
            file = yaml.load(f, Loader=yaml.FullLoader)

        for var_name in file:
            properties = file[var_name]

            if var_name in self:
                raise Exception("Duplicated definition for '{0}'".format(var_name))

            if not "branch" in properties:
                raise Exception("No branch specified for '{0}'".format(var_name))

            var = Variable(var_name, properties["branch"])

            # Split branch by ':'
            # but we have to avoid treating '::' as delimiter, therefore we use RE
            branches = re.split("(?<!:):(?!:)", properties["branch"])
            if len(branches) == 1:
                var.is_2D = False
            elif len(branches) == 2:
                var.is_2D = True
            else:
                raise Exception("The dimension of '{0}' is too high. I only know how to make 1D and 2D histograms!".format(var_name))

            var.length = find(properties, 'length', 1)

            if not var.is_2D:
                var.field_x = find(properties, 'field', var_name)
                var.label_x = find(properties, 'label', "")
                var.root_label_x = find(properties, 'root_label', "")
                var.min_x = find(properties, 'min', None)
                var.max_x = find(properties, 'max', None)
                var.unit_x = find(properties, 'unit', "")
                var.merge_overflow_x = find(properties, 'merge_overflow', False)
                var.merge_underflow_x = find(properties, 'merge_underflow', False)
                var.log_x = find(properties, 'log_x', False)
                var.log_y = find(properties, 'log_y', False)

                # Binning
                if "bin_edges" in properties:
                    if "n_bins" in properties:
                        print "WARNING: 'bin_edges' overriding 'n_bins' for variable '{}'".format(var_name)
                    if "bin_size" in properties:
                        print "WARNING: 'bin_edges' overriding 'bin_size' for variable '{}'".format(var_name)
                    var.setBinEdgesX(properties["bin_edges"])
                elif "bin_size" in properties:
                    if "n_bins" in properties:
                        print "WARNING: 'bin_size' overriding 'n_bins' for variable '{}'".format(var_name)
                    var.setBinSizeX(properties["bin_size"])
                elif "n_bins" in properties:
                    var.setNBinsX(properties["n_bins"])
                else:
                    raise Exception("No binning information found for variable '{}'!".format(var_name))

            else:
                var.field_x = find(properties, 'field_x', var_name.split('_vs_')[0])
                var.field_y = find(properties, 'field_y', var_name.split('_vs_')[1])
                var.label_x = find(properties, 'label_x', "")
                var.label_y = find(properties, 'label_y', "")
                var.root_label_x = find(properties, 'root_label_x', "")
                var.root_label_y = find(properties, 'root_label_y', "")
                var.min_x = find(properties, 'min_x', None)
                var.min_y = find(properties, 'min_y', None)
                var.max_x = find(properties, 'max_x', None)
                var.max_y = find(properties, 'max_y', None)
                var.unit_x = find(properties, 'unit_x', "")
                var.unit_y = find(properties, 'unit_y', "")
                var.merge_overflow_x = find(properties, 'merge_overflow_x', False)
                var.merge_overflow_y = find(properties, 'merge_overflow_y', False)
                var.merge_underflow_x = find(properties, 'merge_underflow_x', False)
                var.merge_underflow_y = find(properties, 'merge_underflow_y', False)
                var.log_x = find(properties, 'log_x', False)
                var.log_y = find(properties, 'log_y', False)
                var.log_z = find(properties, 'log_z', False)

                # Binning
                if "bin_edges_x" in properties:
                    if "n_bins_x" in properties:
                        print "WARNING: 'bin_edges_x' overriding 'n_bins_x' for variable '{}'".format(var_name)
                    if "bin_size_x" in properties:
                        print "WARNING: 'bin_edges_x' overriding 'bin_size_x' for variable '{}'".format(var_name)
                    var.setBinEdgesX(properties("bin_edges_x"))
                elif "bin_size_x" in properties:
                    if "n_bins_x" in properties:
                        print "WARNING: 'bin_size_x' overriding 'n_bins_x' for variable '{}'".format(var_name)
                    var.setBinSizeX(properties("bin_size_x"))
                elif "n_bins_x" in properties:
                    var.setNBinsX(properties("n_bins_x"))
                else:
                    raise Exception("No x-axis binning information found for variable '{}'!".format(var_name))

                if "bin_edges_y" in properties:
                    if "n_bins_y" in properties:
                        print "WARNING: 'bin_edges_y' overriding 'n_bins_y' for variable '{}'".format(var_name)
                    if "bin_size_y" in properties:
                        print "WARNING: 'bin_edges_y' overriding 'bin_size_y' for variable '{}'".format(var_name)
                    var.setBinEdgesY(properties("bin_edges_yx"))
                elif "bin_size_y" in properties:
                    if "n_bins_y" in properties:
                        print "WARNING: 'bin_size_y' overriding 'n_bins_y' for variable '{}'".format(var_name)
                    var.setBinSizeY(properties("bin_size_y"))
                elif "n_bins_y" in properties:
                    var.setNBinsY(properties("n_bins_y"))
                else:
                    raise Exception("No y-axis binning information found for variable '{}'!".format(var_name))

            self[var_name] = var

        print "Initialised variable database: found {:d} variables.".format(len(self))

    def __getitem__(self, key):
        if not key in self:
            raise KeyError("Variable '{0}' not defined!".format(key))
        return super(Variables, self).__getitem__(key)

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} variables ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self),
        ", ".join(v for v in self) )

#-------------------------------------------------------------------------------
# Class of 1D/2D variable
class Variable(object):

    def __init__(self, name, branch):

        self.name = name
        self.branch = branch
        self.length = 1
        self.field_x = ""
        self.field_y = ""
        self.label_x = ""
        self.label_y = ""
        self.root_label_x = ""
        self.root_label_y = ""
        self.min_x = 0
        self.min_y = 0
        self.max_x = 1
        self.max_y = 1
        self.unit_x = ""
        self.unit_y = ""
        self.merge_overflow_x = False
        self.merge_overflow_y = False
        self.merge_underflow_x = False
        self.merge_underflow_y = False
        self.log_x = False
        self.log_y = False
        self.log_z = False
        self.is_derived = False
        self.is_2D = False

        # Binning
        self._n_bins_x = None
        self._n_bins_y = None
        self._bin_size_x = None
        self._bin_size_y = None
        self._bin_edges_x = None
        self._bin_edges_y = None
        self.custom_binning_x = False
        self.custom_binning_y = False

    def setNBinsX(self, n):
        if self._n_bins_x == n:
            return
        self._n_bins_x = n
        self._bin_size_x = None
        self._bin_edges_x = None
        self.custom_binning_x = False

    def setBinSizeX(self, size):
        self._n_bins_x = None
        self._bin_size_x = size
        self._bin_edges_x = None
        self.custom_binning_x = False

    def setBinEdgesX(self, edges):
        self._n_bins_x = len(edges) - 1
        self._bin_size_x = None
        self._bin_edges_x = edges
        self.min_x = edges[0]
        self.max_x = edges[-1]
        self.custom_binning_x = True

    def setNBinsY(self, n):
        if self._n_bins_y == n:
            return
        self._n_bins_y = n
        self._bin_size_y = None
        self._bin_edges_y = None
        self.custom_binning_y = False

    def setBinSizeY(self, size):
        self._n_bins_y = None
        self._bin_size_y = size
        self._bin_edges_y = None
        self.custom_binning_y = False

    def setBinEdgesY(self, edges):
        self._n_bins_y = len(edges) - 1
        self._bin_size_y = None
        self._bin_edges_y = edges
        self.min_y = edges[0]
        self.max_y = edges[-1]
        self.custom_binning_y = True

    @property
    def n_bins_x(self):
        if self._n_bins_x is None:
            if self._bin_size_x is not None:
                self._n_bins_x = int(round((self.max_x-self.min_x) / float(self._bin_size_x)))
            elif self._bin_edges_x is not None:
                self._n_bins_x = len(self._bin_edges_x) - 1
        return self._n_bins_x

    @property
    def bin_size_x(self):
        if self._bin_size_x is None and self._n_bins_x is not None and not self.custom_binning_x:
            self._bin_size_x = (self.max_x-self.min_x) / float(self._n_bins_x)
        return self._bin_size_x

    @property
    def bin_edges_x(self):
        if self._bin_edges_x is None:
            self._bin_edges_x = np.linspace(self.min_x, self.max_x, self.n_bins_x+1)
        return self._bin_edges_x

    @property
    def n_bins_y(self):
        if self._n_bins_y is None:
            if self._n_bin_size_y is not None:
                self._n_bins_y = int(round((self.max_y-self.min_y) / float(self._bin_size_y)))
            elif self._bin_edges_y is not None:
                self._n_bins_y = len(self._bin_edges_y) - 1
        return self._n_bins_y

    @property
    def bin_size_y(self):
        if self._bin_size_y is None and self._n_bins_y is not None and not self.custom_binning_y:
            self._bin_size_y = (self.max_y-self.min_y) / float(self._n_bins_y)
        return self._bin_size_y

    @property
    def bin_edges_y(self):
        if self._bin_edges_y is None:
            self._bin_edges_y = np.linspace(self.min_y, self.max_y, self.n_bins_y+1)
        return self._bin_edges_y

    def instance(self, idx):
        # Replace all TauJetsAuxDyn.FOO_BAR with TauJetsAuxDyn.FOO_BAR[i]
        # The idea is to identify anything between a "TauJetsAuxDyn." and a special character (except '_')
        # and then add "[i]" behind it
        split = self.branch.split("TauJetsAuxDyn.")
        for i,x in enumerate(split[1:]):
            var = re.split(r'[`\-=~!@#$%^&*()+\[\]{};\'\\:"|<,./<>?]', x)[0]
            split[i+1] = x.replace(var, var+"[{0:d}]")
        return "TauJetsAuxDyn.".join(split).format(idx)

    @property
    def field(self): return None if self.is_2D else self.field_x
    @property
    def label(self): return None if self.is_2D else self.label_x
    @property
    def root_label(self): return None if self.is_2D else self.root_label_x
    @property
    def min(self): return None if self.is_2D else self.min_x
    @property
    def max(self): return None if self.is_2D else self.max_x
    @property
    def n_bins(self): return None if self.is_2D else self.n_bins_x
    @property
    def bin_size(self): return None if self.is_2D else self.bin_size_x
    @property
    def bin_edges(self): return None if self.is_2D else self.bin_edges_x
    @property
    def custom_binning(self): return None if self.is_2D else self.custom_binning_x
    @property
    def unit(self): return None if self.is_2D else self.unit_x
    @property
    def merge_overflow(self): return None if self.is_2D else self.merge_overflow_x
    @property
    def merge_underflow(self): return None if self.is_2D else self.merge_underflow_x

    def __str__(self):
        return self.branch

    def __repr__(self):
        return "<{}.{} object at {}>: Variable '{}' = '{}'".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        self.name,
        self.branch )
