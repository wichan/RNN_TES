#!/usr/bin/env python

#######################################################################
### Define custom activation/loss functions,                        ###
### and hack the Keras activations.get and objectives.get functions ###
### such that it can correctly load models with custom objects      ###
#######################################################################

import numpy as np

from keras import backend as K
from keras.models import load_model as keras_load_model
from keras.activations import get as get_default_activations
from keras.objectives import get as get_default_objectives

from mock import patch


#-------------------------------------------------------------------------------
# Custom activation functions
class CustomActivations(dict):
    def __init__(self):
        pass
    
    def __getitem__(self, key):
        try:
            return getattr(self, key)
        except AttributeError:
            return super(CustomActivations, self).__getitem__(key)
    
    @staticmethod
    def leaky_relu(x):
        return K.relu(x, alpha=0.1, max_value=0.0)

    @staticmethod
    def leakier_relu(x):
        return K.relu(x, alpha=0.3, max_value=0.0)

    @staticmethod
    def leaky_tanh(x):
        return K.tanh(x) + x*0.01

    @staticmethod
    def leaky_tanh_plus_1(x):
        return (K.tanh(x) + x*0.01) + 1

    @staticmethod
    def leaky_sigmoid(x):
        return K.sigmoid(x) + x*0.01

# Create instance
custom_activations = CustomActivations()

#-------------------------------------------------------------------------------
# Custom loss functions
class CustomObjectives(dict):
    def __init__(self):
        self['msre'] = self.mean_squared_relative_error
        self['MSRE'] = self.mean_squared_relative_error
    
    def __getitem__(self, key):
        try:
            return getattr(self, key)
        except AttributeError:
            return super(CustomObjectives, self).__getitem__(key)
    
    @staticmethod
    def mean_squared_relative_error(y_true, y_pred):
        diff = K.square((y_true - y_pred) / K.clip(K.abs(y_true), K.epsilon(), np.inf))
        return K.mean(diff, axis=-1)

# Create instance
custom_objectives = CustomObjectives()

#-------------------------------------------------------------------------------
# Custom objects
class CustomObjects(CustomActivations, CustomObjectives, dict):
    def __init__(self):
        self['msre'] = self.mean_squared_relative_error
        self['MSRE'] = self.mean_squared_relative_error

# Create instance
custom_objects = CustomObjects()

#-------------------------------------------------------------------------------
# Patch load model function
def get_activations(x):
    if x in custom_activations:
        return custom_activations[x]
    else:
        return get_default_activations(x)

def get_objectives(x):
    if x in custom_objectives:
        return custom_objectives[x]
    else:
        return get_default_objectives(x)

@patch('keras.activations.get', get_activations)
@patch('keras.objectives.get', get_objectives)
def load_model(file):
    return keras_load_model(file, custom_objects=custom_objects)

