import os
import numpy as np
from array import array
#from scipy import optimize as op

from ROOT import *
from root_utils import *
from general_utils import DefaultOrderedDict

#-------------------------------------------------------------------------------
def evaluatePerformance(responses, var_arr, var):
    coreRes = DefaultOrderedDict(list)
    tailRes = DefaultOrderedDict(list)
    nonClos = DefaultOrderedDict(list)
    for low,high in zip(var.bin_edges[:-1], var.bin_edges[1:]):
        mask = (var_arr>low) & (var_arr<high)
        for tes in responses:
            R = responses[tes][mask]
            if len(R) == 0:
                #print "WARNING: bin {}<{}<{} has zero entry! Setting bin content to zero.".format(low, var.name, high)
                core, tail, nc = 0, 0, 0
            else:
                core, tail, q = findResolution(R)
                #nc = findNonClosure(R)
                nc = np.mean(R[(R>q[1]) & (R<q[2])]) - 1
            coreRes[tes].append(core)
            tailRes[tes].append(tail)
            nonClos[tes].append(nc)
    return coreRes, tailRes, nonClos

#-------------------------------------------------------------------------------
def drawPerformance(responses, var_arr, var, file_prefix):
    coreRes, tailRes, nonClos = evaluatePerformance(responses, var_arr, var)

    basename = os.path.split(file_prefix)[1]
    for tes in responses:
        hist_name = "h_{}_{}_coreRes_{}".format(basename, tes, var.name)
        coreRes[tes] = arrayToRootHist(hist_name, var.bin_edges, coreRes[tes])
        hist_name = "h_{}_{}_tailRes_{}".format(basename, tes, var.name)
        tailRes[tes] = arrayToRootHist(hist_name, var.bin_edges, tailRes[tes])
        hist_name = "h_{}_{}_nonClos_{}".format(basename, tes, var.name)
        nonClos[tes] = arrayToRootHist(hist_name, var.bin_edges, nonClos[tes])

    file_name = "{}_coreRes_{}.pdf".format(file_prefix, var.name)
    drawCoreResolution(coreRes, var, file_name)
    file_name = "{}_tailRes_{}.pdf".format(file_prefix, var.name)
    drawTailResolution(tailRes, var, file_name)
    file_name = "{}_nonClos_{}.pdf".format(file_prefix, var.name)
    drawNonClosure(nonClos, var, file_name)

    root_file_name = file_prefix + ".root"
    with root_open(root_file_name, "update") as f:
        for h in coreRes.values()+tailRes.values()+nonClos.values():
            h.SetDirectory(f)
            h.Write()

#-------------------------------------------------------------------------------
def findResolution(responses, inclusion=(68,95)):
    quantiles = [0.5-inclusion[1]/200.,
                 0.5-inclusion[0]/200.,
                 0.5+inclusion[0]/200.,
                 0.5+inclusion[1]/200.]
    q = np.quantile(responses, quantiles)
    core = (q[2] - q[1]) / 2.
    tail = (q[3] - q[0]) / 2.
    return core, tail, q

#-------------------------------------------------------------------------------
# Non-closure = deviation of peak of the response from unity
# Peak is found by counting number of samples in a sliding window of constant width
def findNonClosure(responses, window=0.1):
    count_in_window = lambda x, R=responses, w=0.5*window: np.count_nonzero(np.abs(R-x)<w)
    count_in_window = np.vectorize(count_in_window)
    max = findMaximum(count_in_window)
    return max - 1

#-------------------------------------------------------------------------------
def drawCoreResolution(hists, var, file_name):
    basename = os.path.splitext(os.path.split(file_name)[0])[0]
    c = createCanvas(basename)

    frame = createFrame(hists.values()[0])
    if var.unit:
        frame.GetXaxis().SetTitle("{} [{}]".format(var.root_label, var.unit))
    else:
        frame.GetXaxis().SetTitle(var.root_label)
    frame.GetYaxis().SetTitle("Resolution (68% coverage)")
    frame.SetMaximum(0.25)
    frame.SetMinimum(0)
    frame.Draw()

    leg = createLegend()
    for i,tes in enumerate(hists):
        hists[tes].SetLineWidth(2)
        hists[tes].SetLineColor(HistColours[i])
        hists[tes].SetMarkerColor(HistColours[i])
        hists[tes].SetMarkerStyle(HistMarkers[i])
        hists[tes].Draw("EPL same")
        leg.AddEntry(hists[tes], tes, "LP")
    leg.Draw()

    atlas_label = atlasLabel("Simulation Internal")
    atlas_label.Draw()

    if var.log_x:
        c.SetLogx()

    c.SaveAs(file_name)

#-------------------------------------------------------------------------------
def drawTailResolution(hists, var, file_name):
    basename = os.path.splitext(os.path.split(file_name)[0])[0]
    c = createCanvas(basename)

    frame = createFrame(hists.values()[0])
    if var.unit:
        frame.GetXaxis().SetTitle("{} [{}]".format(var.root_label, var.unit))
    else:
        frame.GetXaxis().SetTitle(var.root_label)
    frame.GetYaxis().SetTitle("Resolution (95% coverage)")
    frame.SetMaximum(0.5)
    frame.SetMinimum(0)
    frame.Draw()

    leg = createLegend()
    for i,tes in enumerate(hists):
        hists[tes].SetLineWidth(2)
        hists[tes].SetLineColor(HistColours[i])
        hists[tes].SetMarkerColor(HistColours[i])
        hists[tes].SetMarkerStyle(HistMarkers[i])
        hists[tes].Draw("EPL same")
        leg.AddEntry(hists[tes], tes, "LP")
    leg.Draw()

    atlas_label = atlasLabel("Simulation Internal")
    atlas_label.Draw()

    if var.log_x:
        c.SetLogx()

    c.SaveAs(file_name)

#-------------------------------------------------------------------------------
def drawNonClosure(hists, var, file_name):
    basename = os.path.splitext(os.path.split(file_name)[0])[0]
    c = createCanvas(basename)

    frame = createFrame(hists.values()[0])
    if var.unit:
        frame.GetXaxis().SetTitle("{} [{}]".format(var.root_label, var.unit))
    else:
        frame.GetXaxis().SetTitle(var.root_label)
    frame.GetYaxis().SetTitle("Non-closure")
    frame.SetMaximum(0.2)
    frame.SetMinimum(-0.1)
    frame.Draw()

    l = TLine()
    l.SetLineColor(kBlack)
    l.SetLineStyle(2)
    l.DrawLine(var.min, 0, var.max, 0)

    leg = createLegend()
    for i,tes in enumerate(hists):
        hists[tes].SetLineWidth(2)
        hists[tes].SetLineColor(HistColours[i])
        hists[tes].SetMarkerColor(HistColours[i])
        hists[tes].SetMarkerStyle(HistMarkers[i])
        hists[tes].Draw("EPL same")
        leg.AddEntry(hists[tes], tes, "LP")
    leg.Draw()

    atlas_label = atlasLabel("Simulation Internal")
    atlas_label.Draw()

    if var.log_x:
        c.SetLogx()

    c.SaveAs(file_name)

#-------------------------------------------------------------------------------
def findMaximum(f, x0=1, range=0.1, x_threshold=1e-5, y_threshold=1):
    x = x0
    for i in xrange(100):
        if range < x_threshold:
            return x
        x_grid = np.linspace(x-range, x+range, num=11)
        f_grid = np.c_[x_grid, f(x_grid)]
        f_grid = sorted(f_grid, key=lambda x:x[1])
        if f_grid[-1][1] - f_grid[0][1] < y_threshold:
            return x
        else:
            x = f_grid[-1][0]
            range /= 5.
    return x
