import os
import json
from collections import OrderedDict, Callable

#-------------------------------------------------------------------------------
def expandPath(path):
    return os.path.expanduser(os.path.expandvars(path))

#-------------------------------------------------------------------------------
def makeDirs(dir):
    if dir == "":
        return
    dir = expandPath(dir)
    if not os.path.exists(dir):
        os.makedirs(dir)
    elif not os.path.isdir(dir):
        raise NotADirectoryError("Cannot create directory '{}' because the path exists and is not a directory!".format(dir))

#-------------------------------------------------------------------------------
def find(d, key, default=None):
    # This slightly improves readability when things have long names
    return d[key] if key in d else default

#-------------------------------------------------------------------------------
def json_save(path, data):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    else:
        makeDirs(os.path.split(path)[0])
    with open(path, 'w') as f:
        json.dump(data, f)

#-------------------------------------------------------------------------------
def json_load(path):
    with open(path) as f:
        data = json.load(f)
    return data

from collections import OrderedDict, Callable

#-------------------------------------------------------------------------------
class DefaultOrderedDict(OrderedDict):
    # Source: http://stackoverflow.com/a/6190500/562769
    def __init__(self, default_factory=None, *a, **kw):
        if (default_factory is not None and not isinstance(default_factory, Callable)):
            raise TypeError('first argument must be callable')
        OrderedDict.__init__(self, *a, **kw)
        self.default_factory = default_factory

    def __getitem__(self, key):
        try:
            return OrderedDict.__getitem__(self, key)
        except KeyError:
            return self.__missing__(key)

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):
        if self.default_factory is None:
            args = tuple()
        else:
            args = self.default_factory,
        return type(self), args, None, None, self.items()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return type(self)(self.default_factory, self)

    def __deepcopy__(self, memo):
        import copy
        return type(self)(self.default_factory,
                          copy.deepcopy(self.items()))

    def __repr__(self):
        return 'OrderedDefaultDict(%s, %s)' % (self.default_factory, OrderedDict.__repr__(self))
