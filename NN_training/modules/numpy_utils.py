import os
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from general_utils import makeDirs

#-------------------------------------------------------------------------------
# Force np.save() to remove existing file
# Create intermediate directories if not already exist
def np_save(path, array):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    else:
        makeDirs(os.path.split(path)[0])
    np.save(path, array)

#-------------------------------------------------------------------------------
def np_load(path, **kwargs):
    default_kwargs = {"allow_pickle": True}
    default_kwargs.update(kwargs)
    kwargs = default_kwargs

    with open(path) as f:
        arr = np.load(f, **kwargs)
    return arr

#-------------------------------------------------------------------------------
def createStructuredArray(field_names, field_values):
    arr = np.array(field_values[0], dtype=[(field_names[0], field_values[0].dtype.str)])
    if len(field_names) > 1:
        arr = append_fields(arr, field_names[1:], field_values[1:], usemask=False)
    return arr

#-------------------------------------------------------------------------------
def reorderStructuredArray(arr):
    field_names = list(arr.dtype.names)
    field_names.sort()
    field_values = [arr[name] for name in field_names]
    return createStructuredArray(field_names, field_values)

#-------------------------------------------------------------------------------
def flattenStructuredArray(arr, reorder=False):
    field_names = list(arr.dtype.names)
    if reorder:
        field_names.sort()

    field_values = [np.hstack(arr[field]) for field in field_names]
    if not all(len(field_values[0])==len(x) for x in field_values[1:]):
        raise Exception("ERROR: Cannot flatten array - fields have non-uniform lengths!")

    return createStructuredArray(field_names, field_values)

#-------------------------------------------------------------------------------
def padStructuredArray(arr, pad_lengths, pad_value=-999., reorder=False):
    from keras.preprocessing.sequence import pad_sequences

    field_names = list(arr.dtype.names)
    if reorder:
        field_names.sort()

    field_values = []
    for field in field_names:
        if arr[field].dtype == 'O':
            if field in pad_lengths and pad_lengths[field] > 1:
                padded = pad_sequences(arr[field],
                                       maxlen=pad_lengths[field],
                                       dtype=arr[field][0].dtype,
                                       padding='post',
                                       value=pad_value)
                field_values.append(padded)
            else:
                field_values.append(arr[field].astype(arr[field][0].dtype))
        else:
            field_values.append(arr[field])

    return createStructuredArray(field_names, field_values)

#-------------------------------------------------------------------------------
def thinStructuredArray(arr, keep=[]):
    vars_to_drop = [x for x in arr.dtype.names if x not in keep]
    return drop_fields(arr, vars_to_drop, usemask=False)

#-------------------------------------------------------------------------------
def toNonStructured(arr):
    columns = []
    for field in arr.dtype.names:
        if arr[field].dtype == 'O':
            columns.append(arr[field].astype(arr[field][0].dtype))
        else:
            columns.append(arr[field])

    for c in columns:
        if not c.dtype == columns[0].dtype:
            print "Input structured array does not have a uniform dtype:"
            print [c.dtype for c in columns]
            raise RuntimeError("Input structured array does not have a uniform dtype.")
    return np.column_stack(columns)

#-------------------------------------------------------------------------------
def toNonStructuredSequences(arr, pad_kwargs={}):
    from keras.preprocessing.sequence import pad_sequences

    default_pad_kwargs = {"padding": "post", "value": -999}
    default_pad_kwargs.update(pad_kwargs)
    pad_kwargs = default_pad_kwargs

    columns = []
    for field in arr.dtype.names:
        columns.append(pad_sequences(arr[field], dtype=arr[field][0].dtype, **pad_kwargs))

    mask = None
    for i,c in enumerate(columns):
        if not c.dtype == columns[0].dtype:
            print "Input structured array does not have a uniform dtype:"
            print [c.dtype for c in columns]
            raise RuntimeError("Input structured array does not have a uniform dtype.")
        if c.shape[1] == 1:
            # This is a value common to all inputs in the sequence (e.g. trk_ptJetSeed)
            # Repeat it `length` times and then mask it
            if mask is None:
                mask = next(c for c in columns if c.shape[1]>1) == pad_kwargs["value"]
            c = np.repeat(c, mask.shape[1], axis=1)
            c = np.where(mask, pad_kwargs["value"], c)
            columns[i] = c
    return np.dstack(columns)  # axis 1 is the length of the sequences ("the temporal dimension")

