from ROOT import *
from array import array

#-------------------------------------------------------------------------------
### rootpy isn't happy about missing xAOD dictionaries, and will throw errors
### we implement the following emulators instead

#from rootpy.io import root_open
class root_open(TFile):
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        if self and self.IsOpen():
            self.Close()

#-------------------------------------------------------------------------------
def setRootVerbosityLevel(level="Error"):
    if isinstance(level, str):
        gROOT.ProcessLine("{gErrorIgnoreLevel = k%s;}" % level)
    elif isinstance(level, int):
        gROOT.ProcessLine("{gErrorIgnoreLevel = %d;}" % level)
    else:
        print "WARNING: Don't know how to set ROOT verbosity level to '{}'".format(level)

#-------------------------------------------------------------------------------
# It is sometimes convenient to use a context manager
class rootVerbosityLevel(object):
    def __init__(self, level):
        self.level = level
        x = gROOT.GetGlobal("gErrorIgnoreLevel")
        self.original_level = x.__get__(x)
    def __enter__(self):
        setRootVerbosityLevel(self.level)
    def __exit__(self, type, value, traceback):
        setRootVerbosityLevel(self.original_level)

#-------------------------------------------------------------------------------
def createCanvas(name='c', width=800, height=700, margins=(0.15, 0.10, 0.05, 0.05)):
    c = TCanvas("c", "c", width, height)
    c.SetLeftMargin(margins[0])
    c.SetBottomMargin(margins[1])
    c.SetRightMargin(margins[2])
    c.SetTopMargin(margins[3])
    gPad.SetTickx()
    gPad.SetTicky()
    return c

#-------------------------------------------------------------------------------
def createFrame(hist, xTitleSize=0.042, xLabelSize=0.037, yTitleSize=0.042, yLabelSize=0.037):
    frame = hist.Clone()
    frame.Reset()
    frame.SetLineWidth(0)
    frame.SetMarkerSize(0)
    frame.SetFillStyle(0)
    frame.GetXaxis().SetTitleSize(xTitleSize)
    frame.GetXaxis().SetLabelSize(xLabelSize)
    frame.GetYaxis().SetTitleSize(yTitleSize)
    frame.GetYaxis().SetLabelSize(yLabelSize)
    return frame

#-------------------------------------------------------------------------------
def createLegend(header="", location=(0.65, 0.65, 0.95, 0.92)):
    leg = TLegend(location[0], location[1], location[2], location[3])
    leg.SetFillStyle(0)
    leg.SetLineWidth(0)
    leg.SetMargin(0.20)
    leg.SetTextSize(0.040)
    if header:
        leg.SetHeader("#scale[1.2]{%s}" % header)
    return leg

#-------------------------------------------------------------------------------
def atlasLabel(context="", location=(0.18, 0.84, 0.5, 0.92), two_lines=False):
    pt = TPaveText(location[0], location[1], location[2], location[3], "NDC")
    pt.SetBorderSize(0)
    pt.SetFillStyle(0)
    pt.SetLineStyle(0)
    pt.SetTextSize(0.045)
    pt.SetTextAlign(13)

    if context and two_lines:
        pt.AddText("#font[72]{ATLAS}")
        pt.AddText("#font[42]{%s}" % context)
    elif context:
        pt.AddText("#font[72]{ATLAS} #font[42]{%s}" % context)
    else:
        pt.AddText("#font[72]{ATLAS}")

    return pt

#-------------------------------------------------------------------------------
def arrayToRootHist(hist_name, bin_edges, bin_contents, bin_errors=None):
    hist_name = hist_name.replace(' ', '_')
    n_bins = len(bin_edges) - 1
    bin_edges = array('d', bin_edges)
    h = TH1D(hist_name, "", n_bins, bin_edges)
    for i in xrange(n_bins):
        h.SetBinContent(i+1, bin_contents[i])
        if bin_errors is None:
            h.SetBinError(i+1, 0)
        else:
            h.SetBinError(i+1, bin_errors[i])
    return h

#-------------------------------------------------------------------------------
# Define styles
gROOT.SetBatch()
gStyle.SetOptStat(0)
HistColours = [kBlack, kGreen, kBlue, kRed]
HistMarkers = [kOpenCircle, kOpenSquare, kOpenTriangleUp, kOpenTriangleDown]
