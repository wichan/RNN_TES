#!/usr/bin/env python

import os, sys, argparse
import glob
import numpy as np

from root_numpy import tree2array

from modules.Variables import Variables
from modules.VarLists import VarLists
from modules.Selections import Selections
from modules.general_utils import makeDirs
from modules.root_utils import root_open, setRootVerbosityLevel
from modules.numpy_utils import np_save, flattenStructuredArray

#-------------------------------------------------------------------------------
parser = argparse.ArgumentParser('./rootToNumpy.py')

parser.add_argument("--input-dir", "-i", default="", type=str, required=True, help="The input directory (AOD / MxAOD).")
parser.add_argument("--scan-depth", default="1", type=int, help="Input directory scan depth/")
parser.add_argument("--scan-pattern", default="*.root*", type=str, help="Input directory scan pattern.")

parser.add_argument("--output-dir", default="./npy_arrays", type=str, help="The output directory.")
parser.add_argument("--tag", "-t", default="v00", type=str, help="The version tag.")
parser.add_argument("--selection", default="medium_RNN_ID", type=str, help="The event/object selection.")
parser.add_argument("--thinning", default="basics", type=str, help="The thinning scheme (a VarList).")
parser.add_argument("--max-events", default=0, type=int, help="The maximum number of (preselected) events to process. Non-positive numbers = process all.")

parser.add_argument("--variables-db", default="configs/variables.yaml", type=str, help="The variable config file.")
parser.add_argument("--selections-db", default="configs/selections.yaml", type=str, help="The selection config file.")
parser.add_argument("--varlists-db", default="configs/varlists.yaml", type=str, help="The variable list config file.")

parser.add_argument("--force-recreate", "-f", default=False, action="store_true", help="Force recreation of the numpy array files?")

parser.add_argument("--verbose", "-v", default=False, action="store_true", help="Verbose.")
parser.add_argument("--debug", default=False, action="store_true", help="Debug mode (will not process all events).")

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.variables_db):
        raise Exception("Cannot find variables YAML file '{}'".format(args.variables_db))
    if not os.path.exists(args.selections_db):
        raise Exception("Cannot find selections YAML file '{}'".format(args.selections_db))
    if not os.path.exists(args.varlists_db):
        raise Exception("Cannot find varlists YAML file '{}'".format(args.varlists_db))

#-------------------------------------------------------------------------------
def rootToNumpy(args):

    if not args.verbose:
        setRootVerbosityLevel(3001)  # Supress 'missing xAOD dictionary' errors

    selections = Selections(args.selections_db)
    selection = selections[args.selection]

    variables = Variables(args.variables_db)
    varlists = VarLists(args.varlists_db)
    vars = varlists[args.thinning].listAll()
    vars += varlists["for_selection"].listAll() + varlists["for_evaluation"].listAll()
    vars = sorted(list(set(vars)))

    output_file = "{}.{}.npy".format(args.selection, args.thinning)
    output_file = reduce(os.path.join, [args.output_dir, args.tag, output_file])
    if os.path.exists(output_file) and not args.force_recreate:
        print "Output file '{}' already exists. Existing.".format(output_file)
        print "(Use --force-recreate to force recreation of the output file.)"
        sys.exit(0)

    pattern = reduce(os.path.join, [args.input_dir] + ["*"]*(args.scan_depth-1) + [args.scan_pattern])
    files = glob.glob(pattern)
    if len(files) == 0:
        raise Exception("Cannot find any input file matching '{}'".format(pattern))

    print "Loading from AOD/MxAOD..."

    npy_arrays = []
    n_events_processed = 0
    n_taus_preselected = 0
    n_taus_selected = 0
    for file in files:

        print "  at file '{}'...".format(file)

        with root_open(file) as f:
            t = f.Get("CollectionTree")

            n_events_in_tree = t.GetEntries()
            if args.max_events > 0:
                n_events_to_read = min(n_events_in_tree, args.max_events-n_events_processed)
            else:
                n_events_to_read = n_events_in_tree

            # Use the return value of TTree.Project() to count the number of taus
            # Filling the underflow bin is the fastest
            n_taus_preselected += t.Project("_(1,2,3)",
                                            "TauJetsAuxDyn.IsTruthMatched",
                                            "",                # selection
                                            "",                # option
                                            n_events_to_read)  # nentries

            # Loop over tau instances
            # TODO: This is hugely inefficient, any cleverer ways to flatten vectors (taus in events),
            # but without also flattening vectors of vectors (clusters/tracks in taus)?
            for i in xrange(100):
                for var_name in vars:
                    var = variables[var_name]
                    t.SetAlias(var_name, var.instance(i))

                arr = tree2array(t, vars, stop=n_events_to_read)

                #arr = flattenStructuredArray(arr, reorder=True)

                arr = selection(arr)
                if len(arr) == 0:
                    break
                n_taus_selected += len(arr)

                npy_arrays.append(arr)

        n_events_processed += n_events_to_read
        if args.max_events > 0 and n_events_processed >= args.max_events:
            break

        if args.debug:
            for name in arr.dtype.names:
                print name, arr[name][:5]
            break

    print "Selected {} out of {} taus from {} events.".format(n_taus_selected, n_taus_preselected, n_events_processed)

    concatenated = np.concatenate(npy_arrays)
    np_save(output_file, concatenated)
    print "Created numpy array and saved to '{}'.".format(output_file)

    print "Done."


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    checkArgs(args)
    rootToNumpy(args)
