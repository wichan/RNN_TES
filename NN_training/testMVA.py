#!/usr/bin/env python

import sys, os, argparse
from ROOT import *
import numpy as np
from root_numpy import tree2array
from collections import OrderedDict

from modules.Variables import Variables
from modules.evaluation import drawPerformance
from modules.numpy_utils import flattenStructuredArray

#-------------------------------------------------------------------------------
parser = argparse.ArgumentParser('./testMVA.py')

parser.add_argument("--input-file", "-i", required=True, type=str, help="The input ROOT file.")
parser.add_argument("--input-tree", "-t", default="TestTree", type=str, help="The input tree (include TDirectory's if necessary).")
parser.add_argument("--output-dir", "-o", default=".", type=str, help="The output directory.")
parser.add_argument("--output-prefix", "-p", default="MVA", type=str, help="The output prefix.")
parser.add_argument("--decay-mode", "-m", default="all", type=str, choices=["all", "1p0n", "1p1n", "1pXn", "3p0n", "3pXn"], help="The reconstructed decay mode.")

parser.add_argument("--compare", default=["calo_pt", "pantau_pt", "combined_pt"], type=str, nargs='+', help="Other TES to compare with.")
parser.add_argument("--plot-vars", default=["truth_pt", "truth_pt_high", "pantau_eta", "avg_mu"], type=str, nargs='+', help="Variables to plot resolution and non-closure.")

parser.add_argument("--variables-db", default="configs/variables.yaml", type=str, help="The variable config file.")

#-------------------------------------------------------------------------------
def testMVA(args):
    file = TFile(args.input_file)
    tree = file.Get(args.input_tree)

    variables = Variables(args.variables_db)

    all_vars = list(set(args.compare + args.plot_vars + ["truth_pt", "MVA_pt"]))
    for v in all_vars:
        var = variables[v]
        tree.SetAlias(v, var.branch)
    tree.SetAlias("MVA_pt", "BDTG*TauJetsAuxDyn.pt_combined/1000.")
    #tree.SetAlias("MVA_pt", "TauJetsAuxDyn.ptFinalCalib/1000.")

    selection = {"all": "",
                 "1p0n": "TauJetsAuxDyn.PanTau_DecayMode==0",
                 "1p1n": "TauJetsAuxDyn.PanTau_DecayMode==1",
                 "1pXn": "TauJetsAuxDyn.PanTau_DecayMode==2",
                 "3p0n": "TauJetsAuxDyn.PanTau_DecayMode==3",
                 "3pXn": "TauJetsAuxDyn.PanTau_DecayMode==4",
                }
    arr = tree2array(tree, all_vars, selection[args.decay_mode])
    arr = flattenStructuredArray(arr)

    responses = OrderedDict()
    for tes in args.compare:
        responses[tes.replace("_pt", " TES")] = arr[tes] / arr["truth_pt"]
    responses["MVA TES"] = arr["MVA_pt"] / arr["truth_pt"]

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    for v in args.plot_vars:
        var = variables[v]
        file_prefix = os.path.join(args.output_dir, args.output_prefix+"_"+args.decay_mode)
        drawPerformance(responses, arr[v], var, file_prefix)

    file.Close()

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    testMVA(args)
