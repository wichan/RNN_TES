#!/usr/bin/env python

import sys, os, argparse
import gc
from collections import OrderedDict

import numpy as np
np.random.seed(42)
from numpy.lib.recfunctions import append_fields

from root_numpy import array2tree

import tensorflow.compat.v1 as tf
tf.set_random_seed(420)

import matplotlib
matplotlib.use('Agg')  # No display for running in batch/screen
import matplotlib.pyplot as plt

from modules.Variables import Variables
from modules.Selections import Selections
from modules.VarLists import VarLists

from modules.NN_utils import *
from modules.general_utils import makeDirs, json_load
from modules.root_utils import root_open
from modules.numpy_utils import np_load, thinStructuredArray, flattenStructuredArray
from modules.custom_objects import load_model
from modules.evaluation import drawPerformance


#-------------------------------------------------------------------------------
parser = argparse.ArgumentParser('./testNN.py')

input_args = parser.add_argument_group('inputs')
input_args.add_argument("--input-dir", default="./npy_arrays", type=str, help="The input directory in which the input numpy arrays are stored.")
input_args.add_argument("--tag", "-t", default="v00", type=str, help="The version tag.")
input_args.add_argument("--selection", default="medium_RNN_ID", type=str, help="The event/object selection.")
input_args.add_argument("--thinning", default="basics", type=str, help="The thinning scheme.")
input_args.add_argument("--re-selection", default="", type=str, help="Redo selection. Specify selection to perform on top of the selection done when the input array was created.")
input_args.add_argument("--var-list", default="", type=str, help="The list of variables used as the NN input. If not provided, uses all variables (in the thinning scheme).")

output_args = parser.add_argument_group('outputs')
output_args.add_argument("--output-dir", default="./results", type=str, help="The output directory in which the trained models and validation plots will be stored.")
output_args.add_argument("--model-name", "-m", default="default_model", type=str, help="The test model name.")
output_args.add_argument("--no-output-ntuple", default=False, action='store_true', help="Do not save output ntuple.")

NNarch_args = parser.add_argument_group('NN architecture configs')
NNarch_args.add_argument("--model-type", type=str, choices=['dense','LSTM'], default='dense', help="NN model type.")

NNtrain_args = parser.add_argument_group('NN training configs')
NNtrain_args.add_argument("--target", default="truth_pt", type=str, help="The target variable.")
NNtrain_args.add_argument("--tau-decay", default="incl", choices=["incl", "1p", "3p", "mp", "1p0n", "1p1n", "1pXn", "3p0n", "3pXn"], type=str, help="Tau decay mode.")
NNtrain_args.add_argument("--ratio-mode", default=False, action='store_true', help="pT variables are replaced by ratios to the variable given by `--ratio-mode-base`.")
NNtrain_args.add_argument("--ratio-mode-base", default="combined_pt", type=str, help="Denominator of the pT ratios. Used when `--ratio-mode` is chosen.")
NNtrain_args.add_argument("--ratio-mode-keep-raw-truth", default=False, action='store_true', help="Keep truth pT (target) as raw values instead of ratios.")

NNtest_args = parser.add_argument_group('NN evaluation configs')
NNtest_args.add_argument("--compare", default=["calo_pt", "pantau_pt", "MVA_pt"], type=str, nargs='+', help="Other TES to compare with.")
NNtest_args.add_argument("--plot-vars", default=["truth_pt", "truth_pt_high", "pantau_eta", "actl_mu"], type=str, nargs='+', help="Variables to plot resolution and non-closure.")
NNtest_args.add_argument("--checkpoint", default="best", choices=["best", "last"], type=str, help="Model checkpoint used for testing. 'best' = epoch with lowest validation loss, 'last' = last epoch.")
NNtest_args.add_argument("--skip-ranking", action='store_true', help="Skip evaluating input variable ranking.")

yaml_args = parser.add_argument_group('YAML config files')
yaml_args.add_argument("--variables-db", default="configs/variables.yaml", type=str, help="The variable config file.")
yaml_args.add_argument("--selections-db", default="configs/selections.yaml", type=str, help="The selection config file.")
yaml_args.add_argument("--varlists-db", default="configs/varlists.yaml", type=str, help="The variable list config file.")

other_args = parser.add_argument_group('Other')
other_args.add_argument("--n-threads", default=8, type=int, help="Number of threads used by TensorFlow.")
other_args.add_argument("--verbose", "-v", default=False, action="store_true", help="Verbose.")

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.variables_db):
        raise Exception("Cannot find variables YAML file '{}'".format(args.variables_db))
    if not os.path.exists(args.selections_db):
        raise Exception("Cannot find selections YAML file '{}'".format(args.selections_db))
    if not os.path.exists(args.varlists_db):
        raise Exception("Cannot find varlists YAML file '{}'".format(args.varlists_db))

#-------------------------------------------------------------------------------
def testNN(args, loaded_arr=None):

    if not args.verbose:
        tf.logging.set_verbosity(tf.logging.ERROR)  # This turns off "deprecated" warnings

    setTensorFlowThreads(args.n_threads)  # 'PBS: qsub -l nodes=1:ppn=8'

    variables = Variables(args.variables_db)

    #-------------------------------------------------
    # Load numpy array
    if loaded_arr is None:
        input_file = "{}.{}.npy".format(args.selection, args.thinning)
        input_file = reduce(os.path.join, [args.input_dir, args.tag, input_file])
        arr = np_load(input_file)

        print "Loaded input array with {} training samples.".format(len(arr))

        # Re-selection
        if args.re_selection:
            selection = Selections(args.selections_db)[args.re_selection]
            arr = selection(arr)

        # Pick samples with the right tau decay mode
        arr = selectTauDecayMode(arr, args.tau_decay)
        gc.collect()

        print "Selected {} training samples with the following input variables:".format(len(arr))

    else:
        arr = loaded_arr
        print "The given loaded array has {} samples and the following input variables:".format(len(arr))

    #-------------------------------------------------
    # Define input variables and rename stuff
    if args.re_selection:
        args.selection = args.re_selection

    if not args.var_list:
        args.var_list = args.thinning
    var_list = VarLists(args.varlists_db)[args.var_list]
    input_vars = var_list.listAll()
    dropVariables(input_vars, decay_mode=args.tau_decay)  ### HARD-CODED variable drops
    model_name = "{}_{}".format(args.model_name, args.tau_decay)

    output_dir = reduce(os.path.join, [args.output_dir, args.tag,
                                       "{}_{}".format(args.selection, args.var_list)])
    model_dir = os.path.join(output_dir, "model")
    plots_dir = reduce(os.path.join, [output_dir, "plots", model_name])
    makeDirs(model_dir)
    makeDirs(plots_dir)

    input_vars.sort()

    if args.model_type == 'dense':
        for v in input_vars:
            if v not in arr.dtype.names:
                raise KeyError("Required input field '{}' not found in the input array.".format(v))
            print "  {}".format(v)
    elif args.model_type == 'LSTM':
        normal_vars = []
        cluster_vars = []
        track_vars = []
        if "low_level_clusters" in var_list:
            cluster_vars = sorted(var_list["low_level_clusters"])
        if "low_level_tracks" in var_list:
            track_vars = sorted(var_list["low_level_tracks"])
        if len(cluster_vars) + len(track_vars) == 0:
            raise Exception("Var-list has neither (low-level) cluster or track variables. It is either an error, or you want use `--model-type dense` instead.")
        normal_vars = [v for v in input_vars if not v in cluster_vars+track_vars]

        for v in normal_vars:
            if v not in arr.dtype.names:
                raise KeyError("Required input field '{}' not found in the input array.".format(v))
            print "  {}".format(v)
        for v in cluster_vars:
            if v not in arr.dtype.names:
                raise KeyError("Required input field '{}' not found in the input array.".format(v))
            print "  {}".format(v)
        for v in track_vars:
            if v not in arr.dtype.names:
                raise KeyError("Required input field '{}' not found in the input array.".format(v))
            print "  {}".format(v)

    #-------------------------------------------------
    # Ratio mode: replace pT by pT/base
    if args.ratio_mode and loaded_arr is None:
        ptToPtRatio(arr, args.ratio_mode_base, args.ratio_mode_keep_raw_truth)

    #-------------------------------------------------
    # Normalise inputs using saved pre-scaling info (mean and variance of data)
    scaling_file = "{}.scaling".format(model_name)
    scaling_file = os.path.join(model_dir, scaling_file)

    if os.path.exists(scaling_file):
        print "Found saved scaling information. Normalising inputs..."
        scaling = json_load(scaling_file)
    else:
        raise RuntimeError("Couldn't find saved scaling information ('{}')".format(scaling_file))

    if loaded_arr is None:
        applyScaling(arr, scaling)

    #-------------------------------------------------
    # Split according to event_number
    print "Splitting samples according to event_number..."
    odd  = (arr["event_number"] & 1).astype(bool)
    even = np.logical_not(odd)

    if args.model_type == 'dense':
        target, input = splitTargetInput(arr, args.target, input_vars)
    elif args.model_type == 'LSTM':
        if len(track_vars) == 0:
            target, normal_input, seq_input_1 = splitTargetInput(arr, args.target, normal_vars, cluster_vars)
            seq_input_2 = None
        elif len(cluster_vars) == 0:
            target, normal_input, seq_input_1 = splitTargetInput(arr, args.target, normal_vars, track_vars)
            seq_input_2 = None
        else:
            target, normal_input, seq_input_1, seq_input_2 = splitTargetInput(arr, args.target, normal_vars, cluster_vars, track_vars)

    #-------------------------------------------------
    print "Loading NN models..."
    ckpt = "-ckpt" if args.checkpoint=="best" else ""
    model_name_odd = "{}{}.odd.h5".format(model_name, ckpt)
    model_file_odd = os.path.join(model_dir, model_name_odd)
    model_odd = load_model(model_file_odd)

    model_name_even = "{}{}.even.h5".format(model_name, ckpt)
    model_file_even = os.path.join(model_dir, model_name_even)
    model_even = load_model(model_file_even)

    print "Successfully loaded NN models."

    #-------------------------------------------------
    print "Evaluating..."
    if args.model_type == 'dense':
        input_odd = input[odd]
        input_even = input[even]
    elif args.model_type == 'LSTM':
        input_odd = [normal_input[odd], seq_input_1[odd]]
        input_even = [normal_input[even], seq_input_1[even]]
        if seq_input_2 is not None:
            input_odd.append(seq_input_2[odd])
            input_even.append(seq_input_2[even])

    pred_odd = model_even.predict(input_odd).flatten()
    pred_even = model_odd.predict(input_even).flatten()

    #-------------------------------------------------
    # Rearrange the arrays to be in a consistent manner
    pred = np.concatenate([pred_odd, pred_even])
    target = np.concatenate([target[odd], target[even]])
    arr = np.concatenate([arr[odd], arr[even]])

    pred = revertScaling(pred, scaling, args.target)
    target = revertScaling(target, scaling, args.target)
    arr = revertScaling(arr, scaling)

    # Ratio mode: revert pT/base back to pT
    if args.ratio_mode:
        ptRatioToPt(arr, args.ratio_mode_base, args.ratio_mode_keep_raw_truth)
        if not args.ratio_mode_keep_raw_truth:
            pred = pred * arr[args.ratio_mode_base]
            target = target * arr[args.ratio_mode_base]

    # Keep only neccesary fields and then collect garbage to save some memory
    arr = thinStructuredArray(arr, args.plot_vars + args.compare)
    gc.collect()

    #-------------------------------------------------
    # Find resolution and non-closure in pt/eta/<mu>
    print "Drawing resolution and non-closure..."
    responses = OrderedDict()
    for tes in args.compare:
        responses[tes.replace("_pt", " TES")] = arr[tes] / target
    if args.model_type == "LSTM":
        responses["RNN TES"] = pred / target
    else:
        responses["NN TES"] = pred / target

    ### Forget about decay mode for the moment
    ### variable, bin, TES, res68/res95/nc
    for v in args.plot_vars:
        var = variables[v]
        file_prefix = os.path.join(plots_dir, model_name)
        drawPerformance(responses, arr[var.field], var, file_prefix)

    ##-------------------------------------------------
    #if not args.skip_ranking:
    #    print "Ranking variables according to permutation importance..."
    #
    #    scores = {}
    #    header = structuredArrayHeader(bkg_odd, drop=["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]+args.drop_vars)
    #
    #    for i,v in enumerate(header.dtype.names):
    #        shuffled_input_odd = sam_odd[0].copy()
    #        np.random.shuffle(shuffled_input_odd[:,i])
    #        shuffled_input_even = sam_even[0].copy()
    #        np.random.shuffle(shuffled_input_even[:,i])
    #
    #        #print "  Evaluating ROC AUC score with shuffled '{}'...".format(v)
    #        pred_odd = model_even.predict(shuffled_input_odd)
    #        pred_even = model_odd.predict(shuffled_input_even)
    #        pred = np.concatenate([pred_odd, pred_even])
    #
    #        auc_scores[v] = roc_auc_score(true, pred, sample_weight=weight)
    #        #print "    AUC score = {}".format(auc_scores[v])
    #
    #    out_file = "{}/{}.ranking.txt".format(args.output_dir, os.path.basename(model_name))
    #    with writeOnFile(out_file, echo=True) as f_write:
    #        var_name_len = max(8, max(len(v) for v in auc_scores))
    #        f_write("=" * (23+var_name_len))
    #        f_write((" Ranking  {:^%d}  Importance " % var_name_len).format("Variable"))
    #        f_write("=" * (23+var_name_len))
    #
    #        rank = 0
    #        template = " {:7d}  {:%ds}  {:10f}" % var_name_len
    #        for v,s in sorted(auc_scores.items(), key=lambda x: x[1]):
    #            rank += 1
    #            line = template.format(rank, v, original_auc-s)
    #            f_write(line)
    #        f_write("=" * (23+var_name_len))
    #
    #    print "  Saved '{}/{}.ranking.txt'.".format(args.output_dir, os.path.basename(model_name))

    #-------------------------------------------------
    # Save output ntuple
    if not args.no_output_ntuple:
        if args.model_type == "LSTM":
            arr = append_fields(arr, "RNN_pt", pred.astype(float), usemask=False)
        else:
            arr = append_fields(arr, "NN_pt", pred.astype(float), usemask=False)

        data_dir = reduce(os.path.join, [output_dir, "data", model_name])
        makeDirs(data_dir)
        root_file_name = os.path.join(data_dir, model_name) + ".root"
        with root_open(root_file_name, "recreate") as f:
            t = array2tree(flattenStructuredArray(arr), "tau_energy_scales")
            t.SetDirectory(f)
            t.Write()
        print "Saved predictions to {}".format(root_file_name)

    #-------------------------------------------------
    print "Done!"


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    checkArgs(args)
    testNN(args)
