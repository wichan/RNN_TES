#!/usr/bin/env python

import sys, os, argparse
import gc

import numpy as np
np.random.seed(42)
import tensorflow.compat.v1 as tf
tf.set_random_seed(420)

from modules.Selections import Selections
from modules.VarLists import VarLists

from modules.NN_utils import *
from modules.NN_models import NNDensePlusLSTM
from modules.general_utils import json_save, json_load, makeDirs
from modules.numpy_utils import np_save, np_load

from keras.callbacks import ModelCheckpoint


#-------------------------------------------------------------------------------
parser = argparse.ArgumentParser('./trainNN_LSTM.py')

input_args = parser.add_argument_group('inputs')
input_args.add_argument("--input-dir", default="./npy_arrays", type=str, help="The input directory in which the input numpy arrays are stored.")
input_args.add_argument("--tag", "-t", default="untagged", type=str, help="The version tag.")
input_args.add_argument("--selection", default="medium_RNN_ID", type=str, help="The event/object selection.")
input_args.add_argument("--thinning", default="clusters", type=str, help="The thinning scheme.")
input_args.add_argument("--re-selection", default="", type=str, help="Redo selection. Specify selection to perform on top of the selection done when the input array was created.")
input_args.add_argument("--var-list", default="", type=str, help="The list of variables used as the NN input. If not provided, uses all variables (in the thinning scheme).")

output_args = parser.add_argument_group('outputs')
output_args.add_argument("--output-dir", default="./results", type=str, help="The output directory in which the trained models and validation plots will be stored.")
output_args.add_argument("--model-name", "-m", default="unnamed_model", type=str, help="The output model name.")

NNtrain_args = parser.add_argument_group('NN training configs')
NNtrain_args.add_argument("--target", default="truth_pt", type=str, help="The target variable.")
NNtrain_args.add_argument("--tau-decay", default="incl", choices=["incl", "1p", "3p", "mp", "1p0n", "1p1n", "1pXn", "3p0n", "3pXn"], type=str, help="Tau decay mode.")
NNtrain_args.add_argument("--ratio-mode", default=False, action='store_true', help="pT variables are replaced by ratios to the variable given by `--ratio-mode-base`.")
NNtrain_args.add_argument("--ratio-mode-base", default="combined_pt", type=str, help="Denominator of the pT ratios. Used when `--ratio-mode` is selected.")
NNtrain_args.add_argument("--ratio-mode-keep-raw-truth", default=False, action='store_true', help="Keep truth pT (target) as raw values instead of ratios.")

NNarch_args = parser.add_argument_group('NN architecture configs')
NNarch_args.add_argument("--model-type", type=str, choices=['LSTM'], default='LSTM', help="NN model type.")
NNarch_args.add_argument("--branch1-nodes", default=[64,64,64,64], type=int, nargs='+', help="List of number of nodes in each hidden dense layer in branch 1.")
NNarch_args.add_argument("--branch1-actv", default="relu", type=str, help="Hidden dense layers activations in branch 1.")
NNarch_args.add_argument("--branch2-nodes", default=[64,64,64], type=int, nargs='+', help="List of number of nodes in each hidden dense layer in branch 2.")
NNarch_args.add_argument("--branch2-actv", default="relu", type=str, help="Hidden dense layers activations in branch 2.")
NNarch_args.add_argument("--branch2-LSTM-nodes", default=[64,64], type=int, nargs='+', help="List of number of nodes in each hidden LSTM layer in branch 2.")
NNarch_args.add_argument("--branch2-LSTM-actv", default="relu", type=str, help="Hidden LSTM layers activations in branch 2.")
NNarch_args.add_argument("--branch3-nodes", default=[64,64,64], type=int, nargs='+', help="List of number of nodes in each hidden dense layer in branch 3.")
NNarch_args.add_argument("--branch3-actv", default="relu", type=str, help="Hidden dense layers activations in branch 3.")
NNarch_args.add_argument("--branch3-LSTM-nodes", default=[64,64], type=int, nargs='+', help="List of number of nodes in each hidden LSTM layer in branch 3.")
NNarch_args.add_argument("--branch3-LSTM-actv", default="relu", type=str, help="Hidden LSTM layers activations in branch 3.")
NNarch_args.add_argument("--merged-nodes", default=[128,64,32], type=int, nargs='+', help="List of number of nodes in each hidden dense layer in branch 2.")
NNarch_args.add_argument("--merged-actv", default="relu", type=str, help="Hidden dense layers activations in branch 2.")
NNarch_args.add_argument("--output-actv", default="linear", type=str, help="Output layer activation.")

NNopt_args = parser.add_argument_group('NN optimiser configs')
NNopt_args.add_argument("--epochs", "-E", default=100, type=int, help="The number training epochs.")
NNopt_args.add_argument("--batch", "-B", default=64, type=int, help="The number of training samples per batch.")
NNopt_args.add_argument("--learning-rate", "-lr", default=1e-4, type=float, help="The learning rate of the optimiser (adam).")
NNopt_args.add_argument("--resume-training", action='store_true', default=False, help="Resume training of a previously saved model. (NN architecture will be inherited (including drop-out), while optimiser properties can be changed.)")

# These are only used when calling `testNN.py`
NNtest_args = parser.add_argument_group('NN evaluation configs')
NNtest_args.add_argument("--no-evaluate", default=False, action="store_true", help="Do not call `testNN_dense.py` to evaluate performance.")
NNtest_args.add_argument("--compare", default=["calo_pt", "pantau_pt", "MVA_pt"], type=str, nargs='+', help="Other TES to compare with (only for passing to testing script).")
NNtest_args.add_argument("--plot-vars", default=["truth_pt", "truth_pt_high", "pantau_eta", "actl_mu"], type=str, nargs='+', help="Variables to plot resolution and non-closure (only for passing to testing script).")
NNtest_args.add_argument("--checkpoint", default="best", choices=["best", "last"], type=str, help="Model checkpoint used for testing (only for passing to testing script). 'best' = epoch with lowest validation loss, 'last' = last epoch.")
NNtest_args.add_argument("--skip-ranking", action='store_true', help="Skip evaluating input variable ranking.")
NNtest_args.add_argument("--no-output-ntuple", default=False, action='store_true', help="Do not save output ntuple.")

yaml_args = parser.add_argument_group('YAML config files')
yaml_args.add_argument("--variables-db", default="configs/variables.yaml", type=str, help="The variable config file (only for passing to testing script).")
yaml_args.add_argument("--selections-db", default="configs/selections.yaml", type=str, help="The selection config file.")
yaml_args.add_argument("--varlists-db", default="configs/varlists.yaml", type=str, help="The variable list config file.")

other_args = parser.add_argument_group('Other')
other_args.add_argument("--n-threads", default=8, type=int, help="Number of threads used by TensorFlow.")
other_args.add_argument("--verbose", "-v", default=False, action="store_true", help="Verbose.")

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.variables_db):
        raise Exception("Cannot find variables YAML file '{}'".format(args.variables_db))
    if not os.path.exists(args.selections_db):
        raise Exception("Cannot find selections YAML file '{}'".format(args.selections_db))
    if not os.path.exists(args.varlists_db):
        raise Exception("Cannot find varlists YAML file '{}'".format(args.varlists_db))

#-------------------------------------------------------------------------------
def trainNN_LSTM(args):

    if not args.verbose:
        tf.logging.set_verbosity(tf.logging.ERROR)  # This turns off "deprecated" warnings

    setTensorFlowThreads(args.n_threads)  # 'PBS: qsub -l nodes=1:ppn=8'

    #-------------------------------------------------
    # Load numpy array
    print "Loading input array..."
    input_file = "{}.{}.npy".format(args.selection, args.thinning)
    input_file = reduce(os.path.join, [args.input_dir, args.tag, input_file])
    arr = np_load(input_file)

    print "Loaded input array with {} training samples.".format(len(arr))

    # Re-selection
    if args.re_selection:
        selection = Selections(args.selections_db)[args.re_selection]
        arr = selection(arr)

    # Pick samples with the right tau decay mode
    arr = selectTauDecayMode(arr, args.tau_decay)
    gc.collect()

    print "Selected {} training samples with the following input variables:".format(len(arr))

    #-------------------------------------------------
    # Define input variables and rename stuff
    if args.re_selection:
        args.selection = args.re_selection

    if not args.var_list:
        args.var_list = args.thinning
    var_list = VarLists(args.varlists_db)[args.var_list]
    input_vars = var_list.listAll()
    dropVariables(input_vars, decay_mode=args.tau_decay)  ### HARD-CODED variable drops
    model_name = "{}_{}".format(args.model_name, args.tau_decay)

    output_dir = reduce(os.path.join, [args.output_dir, args.tag,
                                       "{}_{}".format(args.selection, args.var_list)])
    model_dir = os.path.join(output_dir, "model")
    plots_dir = reduce(os.path.join, [output_dir, "plots", model_name])
    makeDirs(model_dir)
    makeDirs(plots_dir)

    input_vars.sort()
    normal_vars = []
    cluster_vars = []
    track_vars = []
    if "low_level_clusters" in var_list:
        cluster_vars = sorted(var_list["low_level_clusters"])
    if "low_level_tracks" in var_list:
        track_vars = sorted(var_list["low_level_tracks"])
    if len(cluster_vars) + len(track_vars) == 0:
        raise Exception("Var-list has neither (low-level) cluster or track variables. It is either an error, or you want use `trainNN_dense.py` instead.")
    normal_vars = [v for v in input_vars if not v in cluster_vars+track_vars]

    for v in normal_vars:
        if v not in arr.dtype.names:
            raise KeyError("Required input field '{}' not found in the input array.".format(v))
        print "  {}".format(v)
    for v in cluster_vars:
        if v not in arr.dtype.names:
            raise KeyError("Required input field '{}' not found in the input array.".format(v))
        print "  {}".format(v)
    for v in track_vars:
        if v not in arr.dtype.names:
            raise KeyError("Required input field '{}' not found in the input array.".format(v))
        print "  {}".format(v)

    #-------------------------------------------------
    # Ratio mode: replace pT by pT/base
    if args.ratio_mode:
        ptToPtRatio(arr, args.ratio_mode_base, args.ratio_mode_keep_raw_truth)

    #-------------------------------------------------
    # Normalise inputs and save the pre-scaling info (mean and variance of data)
    print "Normalising inputs and saving scaling information..."
    scaling = findScaling(arr, input_vars + [args.target])
    applyScaling(arr, scaling)

    scaling_file = "{}.scaling".format(model_name)
    scaling_file = os.path.join(model_dir, scaling_file)
    json_save(scaling_file, scaling)

    #-------------------------------------------------
    # Split according to event_number
    print "Splitting samples according to event_number..."
    odd  = (arr["event_number"] & 1).astype(bool)
    even = np.logical_not(odd)

    if len(track_vars) == 0:
        target, normal_input, seq_input_1 = splitTargetInput(arr, args.target, normal_vars, cluster_vars)
        seq_input_2 = None
    elif len(cluster_vars) == 0:
        target, normal_input, seq_input_1 = splitTargetInput(arr, args.target, normal_vars, track_vars)
        seq_input_2 = None
    else:
        target, normal_input, seq_input_1, seq_input_2 = splitTargetInput(arr, args.target, normal_vars, cluster_vars, track_vars)

    #-------------------------------------------------
    # Create NN model
    print "Creating NN models..."
    model_odd = NNDensePlusLSTM(branch1_input=normal_input.shape,
                                branch1_dense_nodes=args.branch1_nodes,
                                branch1_dense_actv=args.branch1_actv,
                                branch2_input=seq_input_1.shape,
                                branch2_dense_nodes=args.branch2_nodes,
                                branch2_dense_actv=args.branch2_actv,
                                branch2_LSTM_nodes=args.branch2_LSTM_nodes,
                                branch2_LSTM_actv=args.branch2_LSTM_actv,
                                branch3_input=None if seq_input_2 is None else seq_input_2.shape,
                                branch3_dense_nodes=args.branch2_nodes,
                                branch3_dense_actv=args.branch2_actv,
                                branch3_LSTM_nodes=args.branch2_LSTM_nodes,
                                branch3_LSTM_actv=args.branch2_LSTM_actv,
                                merged_dense_nodes=args.merged_nodes,
                                merged_dense_actv=args.merged_actv,
                                output_actv=args.output_actv,
                                epochs=args.epochs,
                                batch_size=args.batch,
                                learning_rate=args.learning_rate,
                               )
    model_name_odd = "{}.odd.h5".format(model_name)
    model_file_odd = os.path.join(model_dir, model_name_odd)
    if args.resume_training:
        print "Will resume training by loading previously saved model"
        model_odd.load(model_file_odd)
    model_odd.compile()

    model_even = model_odd.clone()
    model_name_even = "{}.even.h5".format(model_name)
    model_file_even = os.path.join(model_dir, model_name_even)
    if args.resume_training:
        model_even.load(model_file_even)
    model_even.compile()

    print "Successfully created NN models."
    gc.collect()

    #-------------------------------------------------
    # Train!
    input_odd = [normal_input[odd], seq_input_1[odd]]
    input_even = [normal_input[even], seq_input_1[even]]
    if seq_input_2 is not None:
        input_odd.append(seq_input_2[odd])
        input_even.append(seq_input_2[even])

    print "Training NN model (odd)..."
    ckpt_file = model_file_odd.replace(model_name, model_name+"-ckpt")
    ckpt = ModelCheckpoint(ckpt_file, monitor='val_loss', save_best_only=True, verbose=1)
    history_odd = model_odd.fit(input_odd, target[odd], validation_data=(input_even, target[even]),
                                callbacks=[ckpt])
    print "Successfully trained NN model (odd). Saving..."
    model_odd.save(model_file_odd)
    gc.collect()

    print "Training NN model (even)..."
    ckpt_file = model_file_even.replace(model_name, model_name+"-ckpt")
    ckpt = ModelCheckpoint(ckpt_file, monitor='val_loss', save_best_only=True, verbose=1)
    history_even = model_even.fit(input_even, target[even], validation_data=(input_odd, target[odd]),
                                  callbacks=[ckpt])
    print "Successfully trained NN model (even). Saving..."
    model_even.save(model_file_even)
    gc.collect()

    #------------------------------
    print "Making validation plots..."

    import matplotlib
    matplotlib.use('Agg')  # No display for running in batch/screen
    import matplotlib.pyplot as plt

    loss_odd = history_odd.history['loss']
    val_loss_odd = history_odd.history['val_loss']
    loss_even = history_even.history['loss']
    val_loss_even = history_even.history['val_loss']

    history_file_name = "{}.history.npy".format(model_name)
    history_file_name = os.path.join(model_dir, history_file_name)
    if args.resume_training and os.path.exists(history_file_name):
        old_history = np_load(history_file_name)
        loss_odd = np.concatenate(loss_odd, old_history[0])
        val_loss_odd = np.concatenate(val_loss_odd, old_history[1])
        loss_even = np.concatenate(loss_even, old_history[2])
        val_loss_even = np.concatenate(val_loss_even, old_history[3])

    # Plot loss
    plt.plot(loss_odd)
    plt.plot(val_loss_odd, ls='--')
    plt.plot(loss_even)
    plt.plot(val_loss_even, ls='--')
    axes = plt.gca()
    #axes.set_ylim([0, axes.get_ylim()[1]])  # force y-range to start at zero
    plt.legend(['training (odd)', 'testing (odd)', 'training (even)', 'testing (even)'], loc=1)
    plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
    plt.ylabel('Loss', horizontalalignment='right', y=1.0)
    ax = plt.gca()
    ax.set_yscale("log")

    plot_file_name = "{}.loss.pdf".format(model_name)
    plt.savefig(os.path.join(plots_dir, plot_file_name))
    plt.close()

    history = np.c_[loss_odd,
                    val_loss_odd,
                    loss_even,
                    val_loss_even]

    np_save(history_file_name, history)

    #------------------------------
    print "Done!"
    gc.collect()

    #------------------------------
    if args.no_evaluate:
        print "Call testNN.py to evaluate performance."
    else:
        print "Calling testNN.py to evaluate performance..."
        from testNN import testNN
        testNN(args, loaded_arr=arr)

    print "Best model's loss = {} at epoch {} (odd) / {} at epoch {} (even)".format(
           min(val_loss_odd),
           np.argmin(val_loss_odd)+1,
           min(val_loss_even),
           np.argmin(val_loss_even)+1)


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    checkArgs(args)
    trainNN_LSTM(args)
